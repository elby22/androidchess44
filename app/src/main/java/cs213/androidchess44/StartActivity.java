package cs213.androidchess44;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;


import android.content.Context;


import java.io.*;
import java.io.FileNotFoundException;


import java.util.ArrayList;

import app.SavedGame;

public class StartActivity extends AppCompatActivity {
    public static ArrayList<SavedGame> gameList = new ArrayList<SavedGame>();
    public static SavedGame temp;
    public Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        context = getApplicationContext();
        load(context);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

    }

    public void newGame(View view){
        Intent intent = new Intent(this, GameActivity.class);
        startActivity(intent);
    }

    public void loadGame(View view){
        Intent intent = new Intent(this, LoadGameActivity.class);
        startActivity(intent);
    }

    public static void save(Context context){

        try

        {
            File file = new File(context.getFilesDir(), "save.bin");
            ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream(file)); //Select where you wish to save the file...

            outputStream.writeObject(gameList);


            outputStream.flush(); // flush the stream to insure all of the information was written to 'save.bin'
            outputStream.close();// close the stream

        }

        catch(Exception ex)

        {


            System.out.println("Not working");
            ex.printStackTrace();

        }

    }

    public static void load(Context context){

        File file = new File(context.getFilesDir(), "save.bin");
        try

        {

            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file));


                gameList = (ArrayList<SavedGame>) ois.readObject();


        }

        catch(Exception ex)

        {
            System.out.println("Not working2");
            ex.printStackTrace();

        }

        if(!gameList.isEmpty()){
            System.out.println("!?!?!?!");
            System.out.println(gameList.size());
            return;
        } else {
            System.out.println("Empty List");
            System.out.println("The size of the list is: " + gameList.size());
            return;
        }

    }

}
