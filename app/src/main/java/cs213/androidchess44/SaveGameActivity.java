package cs213.androidchess44;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Date;

import app.SavedGame;

public class SaveGameActivity extends AppCompatActivity {

    EditText editText;
    Date date;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_save_game);
        Intent intent = getIntent();
        TextView winner = (TextView) findViewById(R.id.winner);
        winner.setText(intent.getStringExtra("WINNER"));
        TextView method = (TextView) findViewById(R.id.method);
        method.setText(intent.getStringExtra("METHOD"));

        date = new Date();
        TextView dateText = (TextView) findViewById(R.id.dateText);
        dateText.setText(date.toString());
    }

    public void saveGame(View view) {
        EditText editText = (EditText) findViewById(R.id.editText);
        String name = editText.getText().toString();
        StartActivity.temp.date = date;
        StartActivity.temp.title = name;
        StartActivity.gameList.add(StartActivity.temp);
        StartActivity.save(getApplicationContext());
        Intent intent = new Intent(this, StartActivity.class);
        startActivity(intent);
    }

    public void cancel(View view){
        Intent intent = new Intent(this, StartActivity.class);
        startActivity(intent);
    }
}
