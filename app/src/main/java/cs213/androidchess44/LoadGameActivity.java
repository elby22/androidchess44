package cs213.androidchess44;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.Collections;
import java.util.Comparator;

import app.SavedGame;

public class LoadGameActivity extends AppCompatActivity {
    ListView listView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_load_game);
        listView = (ListView) findViewById(R.id.listView);
        final ArrayAdapter adapter = new ArrayAdapter(this,
                R.layout.list_text_view, StartActivity.gameList);
        listView.setAdapter(adapter);

        final Context context = this;

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> myAdapter, View myView, int myItemInt, long mylng) {
//              SavedGame selectedFromList = (SavedGame) (listView.getItemAtPosition(myItemInt));
                Intent intent = new Intent();
                intent.setClass(context, GameActivity.class);
                intent.putExtra("INDEX", myItemInt);
                startActivity(intent);
            }
        });
    }


    public void sortDate(View v) {
        Collections.sort(StartActivity.gameList, new Comparator<SavedGame>() {
            @Override
            public int compare(SavedGame lhs, SavedGame rhs) {
                return lhs.date.compareTo(rhs.date);
            }
        });
        final ArrayAdapter adapter = new ArrayAdapter(this,
                R.layout.list_text_view, StartActivity.gameList);
        listView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    public void sortTitle(View v) {
        Collections.sort(StartActivity.gameList, new Comparator<SavedGame>() {
            @Override
            public int compare(SavedGame lhs, SavedGame rhs) {
                return rhs.title.compareTo(lhs.title);
            }
        });
        final ArrayAdapter adapter = new ArrayAdapter(this,
                R.layout.list_text_view, StartActivity.gameList);
        listView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }
}
