package cs213.androidchess44;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;

import app.Game;
import app.SavedGame;
import piece.King;
import piece.*;
public class GameActivity extends AppCompatActivity {
    GridLayout gridLayout;
    Button next;
    Button undoMove;
    Button aiMove;
    Button drawMove;
    Button resignMove;

    Game game = new Game();
    boolean whiteCheck = false;
    boolean blackCheck = false;
    boolean draw = false;
    boolean gameOver = false;
    int tileWidth;
    int selectedID;
    int justMoved = 0;
    TextView info;
    TextView infoAlt;
    SavedGame savedGame;
    int moveIndex = 0;
    ArrayList<Integer> moves = new ArrayList<Integer>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_game);
        gridLayout = (GridLayout) findViewById(R.id.board);
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        info = (TextView) findViewById(R.id.gameInfo);
        infoAlt = (TextView)findViewById(R.id.infoAlt);
        Intent intent = getIntent();
        moveIndex = intent.getIntExtra("INDEX", -1);

        next = (Button)findViewById(R.id.nextMove);
        undoMove = (Button)findViewById(R.id.undoMove);
        aiMove = (Button)findViewById(R.id.aiMove);
        drawMove = (Button)findViewById(R.id.drawMove);
        resignMove = (Button)findViewById(R.id.resignMove);

        infoAlt.setText("");
        undoMove.setEnabled(false);

        if(moveIndex == -1){
            next.setVisibility(View.INVISIBLE);
        }else{
            savedGame = StartActivity.gameList.get(moveIndex);
            moveIndex = 0;
            for(int[] move : savedGame.moves){
                System.out.println(Arrays.toString(move));
            }
        }
        renderBoard();

    }

    private void renderBoard(){

        gridLayout.removeAllViews();
        boolean darkTile = false;
        for(int i = 0; i < 8; i++){
            for (int j = 0; j < 8; j++){
                TextView tile = new TextView(this);
                if(game.board[i][j] != null){
                    Piece piece = game.board[i][j];
                    tile.setText(mapString(piece));
                }else{
                    tile.setText(R.string.empty);
                }

                tile.setId( (10*i) + j );
                tile.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                tile.setIncludeFontPadding(true);
                tile.setPadding(0, -5, 0, -5);
                tile.setTextSize(40);
                tile.setGravity(Gravity.CENTER);
//                tile.setLayoutParams(new TableLayout.LayoutParams(40, 40, 1f));
                if(darkTile) tile.setBackgroundColor(Color.parseColor("#c5c5c5"));
                else tile.setBackgroundColor(Color.parseColor("#FFFFFFFF"));
                darkTile = !darkTile;

                //Set Listeners
                TextView btn = (TextView) tile;

                btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        handleClick(v);
                    }
                });

                gridLayout.addView(tile);
            }
            darkTile = !darkTile;
        }
        if(gameOver){
            aiMove.setEnabled(false);
            resignMove.setEnabled(false);
            drawMove.setEnabled(false);
            undoMove.setEnabled(false);

            Button saveButton = (Button) findViewById(R.id.saveButton);
            saveButton.setVisibility(View.VISIBLE);
        }else if(game.whiteTurn) {
            info.setText("White's Turn");
        }else{
            info.setText("Black's Turn");
        }
    }

    public static int mapString(Piece piece){
        if(piece.isWhite){
            if(Rook.class.isInstance(piece))
                return R.string.rook_white;
            if(Queen.class.isInstance(piece))
                return R.string.queen_white;
            if(Pawn.class.isInstance(piece))
                return R.string.pawn_white;
            if(Knight.class.isInstance(piece))
                return R.string.knight_white;
            if(King.class.isInstance(piece))
                return R.string.king_white;
            if(Bishop.class.isInstance(piece))
                return R.string.bishop_white;
        }else{
            if(Rook.class.isInstance(piece))
                return R.string.rook_black;
            if(Queen.class.isInstance(piece))
                return R.string.queen_black;
            if(Pawn.class.isInstance(piece))
                return R.string.pawn_black;
            if(Knight.class.isInstance(piece))
                return R.string.knight_black;
            if(King.class.isInstance(piece))
                return R.string.king_black;
            if(Bishop.class.isInstance(piece))
                return R.string.bishop_black;
        }
        return 0;
    }

    private void handleClick(View v){

        if(gameOver) return;
     /*   if(justMoved == v.getId()){
            return;
        }*/
        //Process old move
        for(int move : moves){
            if(v.getId() == move){
                if(isYourColor(v)) {
                    next.setEnabled(false);
                    advanceGame(move);
                    return;
                }
            }
        }

        //ensure proper move
        int y = v.getId() / 10;
        int x = v.getId() % 10;

        Piece piece = game.board[y][x];

        if(piece != null){
            if((piece.isWhite && !game.whiteTurn) || (!piece.isWhite && game.whiteTurn))
                return;
        }else{
            return;
        }

        //Clear old color values
        boolean dTile = false;
        for(int i = 0; i < 8; i++){
            for (int j = 0; j < 8; j++) {
                TextView t = (TextView)findViewById((10 * i) + j);
                if(dTile) t.setBackgroundColor(Color.parseColor("#c5c5c5"));
                else t.setBackgroundColor(Color.parseColor("#FFFFFFFF"));
                dTile = !dTile;
            }
            dTile = !dTile;
        }

        v.setBackgroundColor(Color.parseColor("#FF459CFF"));
        selectedID = v.getId();
        moves = game.getLegalMoves(v.getId());

        //paint legal moves
        for(int move : moves){
            TextView dest = (TextView) findViewById(move);
            dest.setBackgroundColor(Color.parseColor("#FFFFB472"));
        }
    }

    private void advanceGame(int move){
        undoMove.setEnabled(true);
        if(game.whiteTurn)
            game.enPassantCheck(true);
        else
            game.enPassantCheck(false);

        int y = selectedID / 10;
        int x = selectedID % 10;

        int[] srcA = {y, x};

        y = move / 10;
        x = move % 10;
        int[] destA = {y, x};
        System.out.println(game.move(srcA, destA, game.whiteTurn));

        //Check for promotion
        Piece movedPiece = game.board[destA[0]][destA[1]];

        //justMoved = destA[0]*10 + destA[1];
        //Figure out promotions
        if(Pawn.class.isInstance(movedPiece)){
            char type = '0';
            char color = '0';
//            if(input.length() == 7) type = input.charAt(6);
            if(destA[0] == 0 && movedPiece.isWhite) color = 'W';
            else if(destA[0] == 7 && !movedPiece.isWhite) color = 'B';
//
            Piece promoted = promotePiece(color);
            if(promoted != null){
                game.board[destA[0]][destA[1]] = promoted;
            }
        }

        if(draw == true){
            game.undoMove();
            game.whiteTurn = !game.whiteTurn;
            draw = false;
        }
        if(game.isCheck(destA, true) && !game.whiteTurn){
            infoAlt.setText("Check");
            if(game.isCheckMate(true)){
                infoAlt.setText("Checkmate");
                info.setText("Black wins!");
                gameOver = true;
            }
            whiteCheck = false;
        }else if(game.isCheck(destA, false) && game.whiteTurn){
            infoAlt.setText("Check");
            if(game.isCheckMate(false)){
                infoAlt.setText("Checkmate");
                info.setText("White wins!");
                gameOver = true;
            }
        }else{
            infoAlt.setText("");
        }

        game.whiteTurn = !game.whiteTurn;
        moves = new ArrayList<>();
        renderBoard();
    }

    public void saveGame(View view){
        Intent intent = new Intent(this, SaveGameActivity.class);
        String winner = info.getText().toString();
        String method = infoAlt.getText().toString();
        intent.putExtra("WINNER", winner);
        intent.putExtra("METHOD", method);
        StartActivity.temp = new SavedGame();
        StartActivity.temp.moves = game.moves;
        startActivity(intent);
    }

    private static Piece promotePiece(char color) {
        if(color == '0') return null;

        boolean white;
        if(color == 'W')
            white = true;
        else
            white = false;

   /*     if(Character.toUpperCase(type) == 'R')
            return new Rook(white);
        if(Character.toUpperCase(type) == 'N')
            return new Knight(white);
        if(Character.toUpperCase(type) == 'B')
            return new Bishop(white);
        if(Character.toUpperCase(type) == 'P')
            return new Pawn(white);
    */
        return new Queen(white);
    }

    public void nextMove(View v){
        moveFromList();
        moveIndex++;
    }

    public void aiMove(View v){
        int[] move = game.autoMove();
        if(move != null){
            selectedID = 10 * move[0];
            selectedID += move[1];
            int dest = 10 * move [2];
            dest += move[3];

            System.out.println(Arrays.toString(move));
            System.out.println(selectedID);
            System.out.println(dest);

            advanceGame(dest);
        }
    }

    public void undoMove(View v){
        game.undoMove();
        undoMove.setEnabled(false);
        moveIndex--;
        renderBoard();
    }

    public void drawMove(View v){
            if(draw == true){
                infoAlt.setText("Draw");
                info.setText("Game Over");
                gameOver = true;
                renderBoard();
            }else {
                draw = true;
                game.whiteTurn = !game.whiteTurn;
                infoAlt.setText("Draw?");
                renderBoard();
            }
    }

    public void resignMove(View v){
        System.out.println("Resign move");
        if(!game.whiteTurn){
            infoAlt.setText("Resignation");
            info.setText("White wins!");
            gameOver = true;
        }else if(game.whiteTurn){
            infoAlt.setText("Resignation");
            info.setText("Black wins!");
            gameOver = true;
        }
        renderBoard();
    }

    private void moveFromList(){


        int[] move = savedGame.moves.get(moveIndex);

        selectedID = 10 * move[0];
        selectedID += move[1];
        int dest = 10 * move [2];
        dest += move[3];

        System.out.println(moveIndex);
        System.out.println(Arrays.toString(move));
        System.out.println(selectedID);
        System.out.println(dest);
//
        advanceGame(dest);
        if(moveIndex + 1 >= savedGame.moves.size()){
            next.setEnabled(false);
        }else{
            next.setEnabled(true);
        }
    }

    private boolean isYourColor(View v){
        if(moves.isEmpty()){
            int y = v.getId() / 10;
            int x = v.getId() % 10;
            Piece piece = game.board[y][x];
            if(piece != null){
                if((piece.isWhite && game.whiteTurn) || (!piece.isWhite && !game.whiteTurn)){
                    return true;
                }
            }
            return false;
        }
        return true;
    }
}
