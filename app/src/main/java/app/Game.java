package app;

import java.util.ArrayList;
import java.util.Random;

import piece.*;

/**
 * This class is an instance of a running game of chess. It contains methods for 
 * maintaining the state of the board and pieces within as well as moves that can be made on the board.
 *
 * @author Elby (egb37)
 * @author Alex (aer112)
 */
public class Game {
	
	public Piece[][] board; 
	public boolean whiteTurn = true;
	boolean checking = false;
	public ArrayList<int[]> moves = new ArrayList<int[]>();
	int[] prevSrc;
	int[] prevDest;
	int enpUndo = -1;
	Piece enpPiece;
	Piece prevPiece;
	boolean pawnFirst = false;
	public boolean castlingLeft = false;
	public boolean castlingRight = false;


	/**
	 * No-arg constructor that initializes the board as specified in the assignment spec.
	 * It adds pieces to the board as well as nulls (which is the absence of a piece)
	 * @author Elby (egb37)
	 */
	public Game(){
		board = new Piece[8][8];
		
		//Black side (its at the top
		board[0][0] = new Rook(false);
		board[0][1] = new Knight(false);
		board[0][2] = new Bishop(false);
		board[0][3] = new Queen(false);
		board[0][4] = new King(false);
		board[0][5] = new Bishop(false);
		board[0][6] = new Knight(false);
		board[0][7] = new Rook(false);
		
		for(int i = 0; i < 8; i++){
			board[1][i] = new Pawn(false);
		}
		
		//Onoccupied space is taken up by nulls;
		for(int i = 0; i < 8; i++){
			board[2][i] = null;
			board[3][i] = null;
			board[4][i] = null;
			board[5][i] = null;
		}

		//White side
		for(int i = 0; i < 8; i++){
			board[6][i] = new Pawn(true);
		}
		
		board[7][0] = new Rook(true);
		board[7][1] = new Knight(true);
		board[7][2] = new Bishop(true);
		board[7][3] = new Queen(true);
		board[7][4] = new King(true);
		board[7][5] = new Bishop(true);
		board[7][6] = new Knight(true);
		board[7][7] = new Rook(true);
		
	}
	
	/**
	 * printBoard() prints the chess board as specified in the instructions.
	 * To be called before or after a move and keeps the players aware of the current state of the board
	 * This method expects no parameters and returns no value.
	 *
	 * @author Elby (egb37)
	 */
	public void printBoard(){
		boolean printBlack = false;
		for(int i = 0; i < 8; i++){
			for(int j = 0; j < 8; j++){
				
				if(this.board[i][j] != null){
					System.out.print(this.board[i][j] + " ");
				}else{
					if(printBlack)
						System.out.print("## ");
					else
						System.out.print("   ");
				}
				printBlack = !printBlack;

			}
			System.out.println(8-i);
			printBlack = !printBlack;
		}
		
		char c = 'a';

		for(int i = 0; i < 8; i++){
			System.out.print(" " + c + " ");
			c++;
		}
		System.out.println();
		System.out.println();

	}
	
	/**
	 * This method checks to see if a move is valid. If the move is valid, it will update the board
	 * and return true. This involves checking if the move puts this player's king in check, which is an invalid move.
	 * If the move is invalid, no move is made and returns false.
	 *
	 * @author Elby (egb37)
	 * @param startCoord	the coordinates on the board of the piece to be moved
	 * @param endCoord	the coordinates on the board of the destination of the move
	 * @param whiteMove	specifies which player's turn it is
	 * @return true if the move is valid and the board is updated, false if invalid.
	 */
	public boolean move(int[] startCoord, int[] endCoord, boolean whiteMove){
		
		Piece thisPiece = board[startCoord[0]][startCoord[1]];
		if(thisPiece == null){
			return false;
		}else if((thisPiece.isWhite && whiteMove) || (!thisPiece.isWhite && !whiteMove)){
			if(Pawn.class.isInstance(thisPiece)){
				if(thisPiece.isFirstMove)
					pawnFirst = true;
				else
					pawnFirst = false;
			}
			if(thisPiece.legalMove(startCoord, endCoord, this)){
				//Check if this move puts the player in check
				Piece thatPiece = board[endCoord[0]][endCoord[1]];
				if(Pawn.class.isInstance(thisPiece)){
					if(thisPiece.isWhite){
						if(thatPiece == null & board[endCoord[0]+1][endCoord[1]] != null){
							enpPiece = board[endCoord[0]+1][endCoord[1]];
							board[endCoord[0]+1][endCoord[1]] = null;
							int tempX, tempY;
							tempX = endCoord[1];
							tempY = endCoord[0]+1;
							tempY = tempY*10;
							enpUndo = tempX+tempY;
						}
					}else{
						if(thatPiece == null & board[endCoord[0]-1][endCoord[1]] != null){
							enpPiece = board[endCoord[0]+1][endCoord[1]];
							board[endCoord[0]-1][endCoord[1]] = null;
							int tempX, tempY;
							tempX = endCoord[1];
							tempY = endCoord[0]-1;
							tempY = tempY*10;
							enpUndo = tempX+tempY;
						}
					}
				}
				//Castling
				if(King.class.isInstance(thisPiece) && Math.abs(startCoord[1]- endCoord[1]) == 2){
					System.out.println("CASTLING ONE BABY");
					if(endCoord[1] == startCoord[1]+2){ //going to the right rook
						castlingRight = true;
						Piece rookPiece = board[endCoord[0]][7];

						board[endCoord[0]][endCoord[1]-1] = rookPiece;
						board[endCoord[0]][7] = null;
						thisPiece.isFirstMove = false;
						rookPiece.isFirstMove = false;

					}else if(endCoord[1] == startCoord[1]-2){ //going to the left rook
						castlingLeft = true;
						Piece rookPiece = board[endCoord[0]][0];

						board[endCoord[0]][endCoord[1]+1] = rookPiece;
						board[endCoord[0]][0] = null;
						thisPiece.isFirstMove = false;
						rookPiece.isFirstMove = false;
					}
					;
				}

				board[startCoord[0]][startCoord[1]] = null;
				board[endCoord[0]][endCoord[1]] = thisPiece;

				
				for(int i = 0; i < 8; i++){
					for(int j = 0; j < 8; j++){
						int[] tempStart = {i, j};
						if(isCheck(tempStart, whiteMove)){
							//If king goes into check, replace move
							if(pawnFirst) thisPiece.isFirstMove = true;
							board[startCoord[0]][startCoord[1]] = thisPiece;
							board[endCoord[0]][endCoord[1]] = thatPiece;		
							return false;
						}
					}
				}
				int[] move = {
					startCoord[0],
					startCoord[1],
					endCoord[0],
					endCoord[1]
				};
				prevSrc = startCoord;
				prevDest = endCoord;
				prevPiece = thatPiece;
				moves.add(move);
				return true;
			}else
				return false;
		}
		return false;
	}
	
	/**
	 * This method translates an input string from player into a set of coordinates used by other methods.
	 * @author Alex (aer112)
	 * @param str	The string to be translated
	 * @return an array of cooridnates from [0,8] in the form [y][x] - file, rank.
	 */
	public static int[] getIndex(String str){
		int x = 0, y;
		int[] coordinate = new int[2];
		
		if(str.charAt(0) == 'a'){
			x = 0;
		}else if(str.charAt(0) == 'b'){
			x = 1;
		}else if(str.charAt(0) == 'c'){
			x = 2;
		}else if(str.charAt(0) == 'd'){
			x = 3;
		}else if(str.charAt(0) == 'e'){
			x = 4;
		}else if(str.charAt(0) == 'f'){
			x = 5;
		}else if(str.charAt(0) == 'g'){
			x = 6;
		}else if(str.charAt(0) == 'h'){
			x = 7;
		}
		
		y = 8 - (str.charAt(1)-'0');
		
		coordinate[0] = y;
		coordinate[1] = x;
	
		return coordinate;
	}
	
	/**
	 * This method determines if a recently moved piece puts a king into check.
	 * 
	 * @author Elby (egb37)
	 * @param startCoord	The coordinates of the piece potentially putting a king in check
	 * @param white 	    True for checking if the white king is in check, false if checking for black king in check.
	 * @return true if the specified piece puts the specified king into check, false otherwise
	 */
	public boolean isCheck(int[] startCoord, boolean white){
		int[] endCoord = getKingCoordinates(white);


		Piece srcPiece = board[startCoord[0]][startCoord[1]];
		if(srcPiece == null) return false;
		checking = true;
		if(srcPiece.legalMove(startCoord, endCoord, this)){
			checking = false;
			return true;
		}
		checking = false;
		return false;
	}
	
	/**
	 * Determines the end of the game. Essentially checks if the king specified can make a legal move to get out of check
	 * returns false if ANY possible move does not put king back into check
	 * 
	 * @author Elby (egb37)
	 * @param white 	    True for checking if the white king is in checkmate, false if checking for black king in checkmate.
	 * @return true if no moves are legal, thus checkmate and false otherwise
	 */
	public boolean isCheckMate(boolean white){
		
		int[] kingCoord = getKingCoordinates(white);
		int[] tempCoord = {kingCoord[0], kingCoord[1]};
		
		//going around the square starting at the left position to the king
		//Doesn't matter if black or white because checks if valid
		//All movements up
		tempCoord[1] = tempCoord[1] - 1;
		if(kingCanMove(kingCoord, tempCoord, white)) return false;
				
		tempCoord[0] = tempCoord[0] - 1;
		if(kingCanMove(kingCoord, tempCoord, white)) return false;

		tempCoord[1] = tempCoord[1] + 1;
		if(kingCanMove(kingCoord, tempCoord, white)) return false;
	
		tempCoord[1] = tempCoord[1] + 1;
		if(kingCanMove(kingCoord, tempCoord, white)) return false;
		
		tempCoord[0] = tempCoord[0] + 1;
		if(kingCanMove(kingCoord, tempCoord, white)) return false;
		
		tempCoord[0] = tempCoord[0] + 1;
		if(kingCanMove(kingCoord, tempCoord, white)) return false;
		
		tempCoord[1] = tempCoord[1] - 1;
		if(kingCanMove(kingCoord, tempCoord, white)) return false;
		
		tempCoord[1] = tempCoord[1] - 1;
		if(kingCanMove(kingCoord, tempCoord, white)) return false;
		
		//Can something take the piece putting it in check?
		
		ArrayList<int[]> dangerousPieces = new ArrayList<int[]>();
		
		for(int i = 0; i < 8; i++){
			for(int j = 0; j < 8; j++){
				int[] tempStart = {i, j};
				Piece dangerousPiece = board[i][j];
				if(dangerousPiece != null && dangerousPiece.legalMove(tempStart, kingCoord, this)){
					dangerousPieces.add(tempStart);
				}
			}
		}

		ArrayList<Integer> attackingMoves = new ArrayList<Integer>();

		//Can something go in the way?
		ArrayList<int[]> nextToKing = new ArrayList<int[]>();
		for(int[] DP : dangerousPieces){
			int x, y;
			y = DP[0]*10;
			x = DP[1];
			if(!Knight.class.isInstance(board[DP[0]][DP[1]]) ){
				attackingMoves = getLegalMoves(x+y);
				for(int m : attackingMoves) {
					y = m / 10;
					x = m % 10;
					if (board[y][x] == null) {
						System.out.println("CURRENT ATTACKING SPACE IS: " + m);
						int kcx = kingCoord[1];
						int kcy = kingCoord[0];
						System.out.println("CURRENT KING COORD IS: " + kingCoord[0] + " " + kingCoord[1]);
						int[] temp = new int[2];

						if (kcx < 7 && board[kcy][kcx + 1] == null && kcy == y && kcx + 1 == x) {
							temp[0] = kcy;
							temp[1] = kcx + 1;
							nextToKing.add(temp);
						} else if (kcx > 0 && board[kcy][kcx - 1] == null && kcy == y && kcx - 1 == x) {
							temp[0] = kcy;
							temp[1] = kcx - 1;
							nextToKing.add(temp);
						} else if (kcy > 0 && board[kcy - 1][kcx] == null && kcy - 1 == y && kcx == x) {
							temp[0] = kcy - 1;
							temp[1] = kcx;
							nextToKing.add(temp);
						} else if (kcy < 7 && board[kcy + 1][kcx] == null && kcy + 1 == y && kcx == x) {
							temp[0] = kcy + 1;
							temp[1] = kcx;
							nextToKing.add(temp);
						} else if (kcy > 0 && kcx > 0 && board[kcy - 1][kcx - 1] == null && kcy - 1 == y && kcx - 1 == x) {
							temp[0] = kcy - 1;
							temp[1] = kcx - 1;
							nextToKing.add(temp);
						} else if (kcy > 0 && kcx < 7 && board[kcy - 1][kcx + 1] == null && kcy - 1 == y && kcx + 1 == x) {
							System.out.println("TEST1");
							temp[0] = kcy - 1;
							temp[1] = kcx + 1;
							nextToKing.add(temp);
						} else if (kcy < 7 && kcx > 0 && board[kcy + 1][kcx - 1] == null && kcy + 1 == y && kcx - 1 == x) {
							System.out.println("TEST2");
							temp[0] = kcy + 1;
							temp[1] = kcx - 1;
							nextToKing.add(temp);
						} else if (kcy < 7 && kcx < 7 && board[kcy + 1][kcx + 1] == null && kcy + 1 == y && kcx + 1 == x) {
							temp[0] = kcy + 1;
							temp[1] = kcx + 1;
							nextToKing.add(temp);
						}
					}
				}
			}
		}
		//You can't dodge this!
		if(nextToKing.size() > 1){return true;}
		if(dangerousPieces.size() > 1 && nextToKing.size() > 1) return true;

		if(!dangerousPieces.isEmpty()){
			int[] danger = dangerousPieces.get(0);
			for(int i = 0; i < 8; i++){
				for(int j = 0; j < 8; j++){
					int[] saviorStart = {i, j};
					Piece saviorPiece = board[i][j];
					if(saviorPiece != null && saviorPiece.legalMove(saviorStart, danger, this)){
						return false; //Something can save it
					}
					if(!nextToKing.isEmpty() && saviorPiece != null && saviorPiece.legalMove(saviorStart, nextToKing.get(0), this)){
						return false; //something can block it
					}
				}
			}
		}

		
		return true;
	}
	
	/**
	 * Iteratively finds the coordinates of the king in question
	 * 
	 * @author Elby (egb37)
	 * @param white	true if finding white king, false if finding black king
	 * @return coordinates of the king of the color specified by the parameter
	 */
	public int[] getKingCoordinates(boolean white){
		for(int i = 0; i < 8; i++){
			for(int j = 0; j < 8; j++){
				Piece thisPiece = board[i][j];
				if(thisPiece != null){
					if(King.class.isInstance(thisPiece)){
						int[] coord = {i, j};
						if(white && thisPiece.isWhite){
							return coord;
						}else if(!white && !thisPiece.isWhite){
							return coord;
						}
					}
				}
			}
		}
		return null;
	}
	/**
	 * This method unvalidates en passant after one turn has passed, as en passant is a 1-turn possibility 
	 * that extinguishes on the next turn of the player who's pawn moved two spaces.
	 * The check is done on all pieces in a row and sets their "enp" values to false, as at that point
	 * the possibility to capture that piece via en passante will have passed
	 * 
	 * @author Alex (aer112)
	 * @param whiteTurn checks if it's white's turn; to keep track of which [y] value to use when checking
	 */
	public void enPassantCheck(boolean whiteTurn){
		if(whiteTurn){
			for(int i = 0; i < 8; i++){
				if(board[4][i] != null && board[4][i].isWhite){
					board[4][i].enp = false;
				}
			}
		}else{
			for(int i = 0; i < 8; i++){
				if(board[3][i] != null && !board[3][i].isWhite){
					board[3][i].enp = false;
				}
			}
		}
	}

	/**
	 * Determines if a king can make a specific move. Used in conjunction with the isCheckMate.
	 * Simulates a move that a king might be able to make and determines if the move can be made.
	 * 
	 * @author Elby (egb37)
	 * @param startCoord	coords of the starting position of the king 
	 * @param endCoord	coords of the potential ending position of the king 
	 * @param white		true for white king, false for black king

	 * @return true if the king can make the move, false if the move is invalid or puts the king in check
	 */	
	public boolean kingCanMove(int[] startCoord, int[] endCoord, boolean white){

		if(endCoord[0] < 0 || endCoord[0] > 7 || endCoord[1] < 0 || endCoord[1] > 7) 
			return false;

		//King origional position
		Piece thisPiece = board[startCoord[0]][startCoord[1]];

		//Check if this move puts the player in check
		Piece thatPiece = board[endCoord[0]][endCoord[1]];
		
		//Empty space that king can move into
		if(thatPiece != null)
			return false;

		checking = true;
		if(thisPiece.legalMove(startCoord, endCoord, this)){
			board[startCoord[0]][startCoord[1]] = null;
			board[endCoord[0]][endCoord[1]] = thisPiece;
			checking = false;
			for(int i = 0; i < 8; i++){
				for(int j = 0; j < 8; j++){
					int[] tempStart = {i, j};
					if(isCheck(tempStart, white)){
												
						//Replace king and announce bad move
						board[startCoord[0]][startCoord[1]] = thisPiece;
						board[endCoord[0]][endCoord[1]] = thatPiece;	

						return false;
					}
				}
			}
			
			//Replace king and announce possible move
			board[startCoord[0]][startCoord[1]] = thisPiece;
			board[endCoord[0]][endCoord[1]] = thatPiece;	
			checking = false;
			return true;
		}else{
			checking = false;
			return false;
		}

	}

	public ArrayList<Integer> getLegalMoves(int id){
		ArrayList<Integer> moves = new ArrayList<Integer>();

		int y = id / 10;
		int x = id % 10;

		int[] srcA = {y, x};
		Piece src = board[y][x];
		boolean addMove = false;
		//boolean pawnFirst = false;

		for(int i = 0; i < 8; i++){
			for(int j = 0; j < 8; j++){
				int[] destA = {i, j};

				if(Pawn.class.isInstance(src) || King.class.isInstance(src) || Rook.class.isInstance(src)){
					if(src.isFirstMove)
						pawnFirst = true;
					else
						pawnFirst = false;
				}

				if(src.legalMove(srcA, destA, this)) {
					//Check if this move puts the player in check
					Piece thatPiece = board[destA[0]][destA[1]];

					board[srcA[0]][srcA[1]] = null;
					board[destA[0]][destA[1]] = src;
					addMove = true;
					for (int ii = 0; ii < 8; ii++) {
						for (int jj = 0; jj < 8; jj++) {
							int[] tempStart = {ii, jj};
							if (isCheck(tempStart, whiteTurn)) {
								addMove = false;
							}
						}
					}
					if (pawnFirst) src.isFirstMove = true;
					board[srcA[0]][srcA[1]] = src;
					board[destA[0]][destA[1]] = thatPiece;
					if(addMove) {
						int newID = (destA[0] * 10) + destA[1];
						moves.add(newID);
					}
					addMove = false;
				}
			}
		}

		return moves;
	}

	public int[] autoMove(){
		ArrayList<Integer> tiles = new ArrayList<Integer>();
		int[] move = new int[4];
		for(int i = 0; i < 8; i++){
			for(int j = 0; j < 8; j++){
				if(board[i][j] == null) continue;
				if( (board[i][j].isWhite & whiteTurn) || (!board[i][j].isWhite & !whiteTurn) ){
					int tile = (i * 10) + j;
					tiles.add(tile);
				}
			}
		}

		while(!tiles.isEmpty()){
			int i = new Random().nextInt(tiles.size());
			ArrayList<Integer> moves = getLegalMoves(tiles.get(i));
			if(moves.isEmpty()){
				tiles.remove(i);
			}else{
				int j = new Random().nextInt(moves.size());
				move[0] = tiles.get(i) / 10;
				move[1] = tiles.get(i) % 10;
				move[2] = moves.get(j) / 10;
				move[3] = moves.get(j) % 10;
				return move;
			}
		}
		return null;
	}

	public void undoMove(){
		//moves.remove(moves.size()-1);
		Piece movedPiece = board[prevDest[0]][prevDest[1]];
		board[prevSrc[0]][prevSrc[1]] = movedPiece;

		//handles undoing castling
		if(King.class.isInstance(movedPiece)){

			if(castlingLeft == true){
				castlingLeft = false;
				board[prevSrc[0]][0] = board[prevSrc[0]][3];
				board[prevSrc[0]][0].isFirstMove = true;
				board[prevSrc[0]][3] = null;

			}else if(castlingRight == true){
				castlingRight = false;
				board[prevSrc[0]][7] = board[prevSrc[0]][5];
				board[prevSrc[0]][7].isFirstMove = true;
				board[prevSrc[0]][5] = null;

			}
		}

		if(Pawn.class.isInstance(movedPiece) || King.class.isInstance(movedPiece) || Rook.class.isInstance(movedPiece)){
			if(pawnFirst) movedPiece.isFirstMove = true;
		}
		if(enpUndo != -1){
			int tempX, tempY;
			tempX = enpUndo%10;
			tempY = enpUndo/10;
			board[tempY][tempX] = enpPiece;
			enpUndo = -1;
			enpPiece = null;
		}
		board[prevDest[0]][prevDest[1]] = prevPiece;
		whiteTurn = !whiteTurn;
	}


	public boolean getChecking(){
		return checking;
	}
}


