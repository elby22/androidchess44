package app;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;

/**
 * Created by Elby on 5/1/2016.
 */

public class SavedGame implements Comparator<SavedGame>, Serializable {
    public ArrayList<int[]> moves;
    public String title;
    public Date date;

//        public SavedGame(String title, ArrayList<int[]> game, Date date) {
//            this.title = title;
//            this.date = date;
//            moves = game;
//        }

    public SavedGame(){
    }

    public String toString(){
        return (title + "\n" + date.toGMTString());
    }

    @Override
    public int compare(SavedGame lhs, SavedGame rhs) {
        return lhs.toString().compareTo(rhs.toString());
    }
}
