package piece;

import app.Game;
/**
 * This class implements the Pawn piece
 * 
 * @author Elby (egb37)
 *
 */
public class Pawn extends Piece {

	public Pawn(boolean isWhite) {
		super(isWhite);
	}
	
	public String toString(){
		if(isWhite){
			return "wp";
		}
		return "bp";
	}

	/**
	 * Checks if a move is legal given the starting and ending coordinates.
	 * If it's the pawn's first move, allows for movement of 2 spaces. 
	 * Does not allow jumping over pieces on first move.
	 * Additionally, en passant is implemented.
	 * @author Elby (egb37)
	 * 
	 * @param src	coords of the starting position of the piece
	 * @param dest	coords of the destination 
	 * @param game	an instance of a game to determine valid moves
	 * @return true if the move if legal
	 * @return false if the move is illegal
	 */
	public boolean legalMove(int[] src, int[] dest, Game game) 
	{	
		//REFACTOR - I mixed up X and Y in this, the concept is not correct
		int srcX = src[1];
		int srcY = src[0];
		int destX = dest[1];
		int destY = dest[0];
		
		Piece destPiece = game.board[destY][destX];
		if(destPiece == null && destX == srcX)
		{
			//I think there's some work to be done here
			if(isWhite){
				if(isFirstMove && (destY == (srcY - 1) || destY == (srcY - 2))){
					if(destY == srcY-2 && game.board[destY+1][destX] != null){
						return false;
					}
					isFirstMove = false;
					enp = true;
					return true;
				}
				if(destY == (srcY - 1)) return true;
				return false;
			}else{        //right here in particular, maybe that should be a 1? there are other things, too
				if(isFirstMove && (destY == (srcY + 1) || destY == (srcY + 2))){
					if(destY == srcY + 2 && game.board[destY-1][destX] != null){
						return false;
					}
					isFirstMove = false;
					enp = true;
					return true;
				}
				if(destY == (srcY + 1)) return true;
				return false;
			}
		}else{ //Attacking
			
			//checks for En Passant
			if(isWhite && srcY == 3 && Math.abs(srcX - destX) == 1 && srcY - destY == 1){
				if(game.board[destY][destX] == null && game.board[destY+1][destX] != null){
					Piece temp = game.board[destY+1][destX];
					if(temp.enp == true && !temp.isWhite){
						//game.board[destY+1][destX].isDead = true;
						//game.board[destY+1][destX] = null;
						return true;
					}
				}
			}else if(!isWhite && srcY == 4 && Math.abs(srcX - destX) == 1 && destY - srcY == 1){
				if(game.board[destY][destX] == null && game.board[destY-1][destX] != null){
					Piece temp = game.board[destY-1][destX];
					if(temp.enp == true && temp.isWhite){
						//game.board[destY-1][destX].isDead = true;
						//game.board[destY-1][destX] = null;
						return true;
					}
				}
			}
			
			//no en Passant, must check for null
			if(game.board[destY][destX] == null){
				return false;
			}
			//Attacking same side?
			if(isWhite && destPiece.isWhite) return false;
			if(!isWhite && !destPiece.isWhite) return false;
			
			if((srcX + 1) == destX || (srcX - 1) == destX){
				if((isWhite && destY == (srcY - 1)) || (!isWhite && destY == (srcY + 1))){
					return true;
			}
				
			return false;
			}
		}
		
		//Shouldn't happen
		return false;
	}

}
