package piece;

import app.Game;
/**
 * This class Implements Queen
 * 
 * @author Alex (aer112)
 */
public class Queen extends Piece {

	public Queen(boolean isWhite) {
		super(isWhite);
	}

	public String toString(){
		if(isWhite){
			return "wQ";
		}
		return "bQ";
	}

	
	
	/**
	 * Checks if a move is legal given the starting and ending coordinates.
	 * First checks for diagonality, then straight-line movement. If the move is diagonal, it's the same code as Bishop's legalMove().
	 * If the move is straight, it's the same code as Rook's legalMove().
	 * 
	 * @author Alex (aer112)
	 * 
	 * @param src	coords of the starting position of the piece
	 * @param dest	coords of the destination 
	 * @param game	an instance of a game to determine valid moves
	 * @return true if the move if legal
	 * @return false if the move is illegal
	 */
	public boolean legalMove(int[] src, int[] dest, Game game) {
		boolean right, up, vertical;
		int srcX = src[1];
		int srcY = src[0];
		int destX = dest[1];
		int destY = dest[0];
		
		//NEED TO CHECK THE ORDER OF [X][Y]
		Piece destPiece = game.board[destY][destX];
		
		//No movement
		if(srcX == destX && srcY == destY){
			return false;
		}
		
		//Queen movement is not horizontal or vertical, same as Bishop
		if(srcX != destX && srcY != destY){
			if(srcX > destX){
				right = false;
			}else{
				right = true;
			}
			
			if(srcY > destY){
				up = true;
			}else{
				up = false;
			}
			
			//checks for diagonallity 
			if(Math.abs(srcX - destX) != Math.abs(srcY - destY)){
				return false;
			}
			
			//not attacking
			if(destPiece == null){
				if(up == true){
					if(right == true){
						//checks that there are no pieces blocking the way for going UP RIGHT
						int j = srcY-1;
						for(int i = srcX+1; i < destX; i++){
							if(game.board[j][i] != null){
								return false;
							}
							j--;
						}
					}else{
						//checks that there are no pieces blocking the way for going UP LEFT
						int j = srcY-1;
						for(int i = srcX-1; i > destX; i--){
							if(game.board[j][i] != null){
								return false;
							}
							j--;
						}
					}
				}else{
					if(right == true){
						//checks that there are no pieces blocking the way for going DOWN RIGHT
						int j = srcY+1;
						for(int i = srcX+1; i < destX; i++){
							if(game.board[j][i] != null){
								return false;
							}
							j++;
						}
					}else{
						//checks that there are no pieces blocking the way for going DOWN LEFT
						int j = srcY+1;
						for(int i = srcX-1; i > destX; i--){			
							if(game.board[j][i] != null){
								return false;
							}
							j++;
						}
					}
				}
			}else{ //attacking
				if(isWhite && destPiece.isWhite) return false;
				if(!isWhite && !destPiece.isWhite) return false;
				if(up == true){
					if(right == true){
						//checks that there are no pieces blocking the way for going UP RIGHT
						int j = srcY-1;
						for(int i = srcX+1; i < destX; i++){
								if(game.board[j][i] != null){
									return false;
								}
							j--;
						}
					}else{
						//checks that there are no pieces blocking the way for going UP LEFT
						int j = srcY-1;
						for(int i = srcX-1; i > destX; i--){
								if(game.board[j][i] != null){
									return false;
								}
							j--;
						}
					}
				}else{
					if(right == true){
						//checks that there are no pieces blocking the way for going DOWN RIGHT
						int j = srcY+1;
						for(int i = srcX+1; i < destX; i++){
							if(game.board[j][i] != null){
								return false;
							}
							j++;
						}
					}else{
						//checks that there are no pieces blocking the way for going DOWN LEFT
						int j = srcY+1;
						for(int i = srcX-1; i > destX; i--){
							if(game.board[j][i] != null){
								return false;
							}
							j++;
						}
					}
				}
			}
			return true;
			
		//Queen movement is horizontal or vertical, same as Rook	
		}else if(srcX == destX || srcY == destY){
			if(srcX == destX && srcY != destY){
				vertical = true;
			}else{
				vertical = false;
			}
			
			if(srcX > destX){
				right = false;
			}else{
				right = true;
			}
			
			if(srcY > destY){
				up = true;
			}else{
				up = false;
			}
			
			if(destPiece == null){ //not attacking
				if(vertical == true){
					if(up == true){ //checks vertically moving upwards; makes sure nothing is in the way
						for(int i = srcY-1; i > destY; i--){
							if(game.board[i][destX] != null){
								return false;
							}
						}
					}else{ //checks vertically moving downwards; makes sure nothing is in the way
						for(int i = srcY+1; i < destY; i++){
							if(game.board[i][destX] != null){
								return false;
							}
						}
					}
				}else{
					if(right == true){ //checks horizontally moving right; makes sure nothing is in the way
						for(int i = srcX+1; i < destX; i++){
							if(game.board[destY][i] != null){
								return false;
							}
						}
					}else{  //checks horizontally moving left; makes sure nothing is in the way
						for(int i = srcX-1; i > destX; i--){
							if(game.board[destY][i] != null){
								return false;
							}
						}
					}
				}
			}else{
				//Attacking same side?
				if(isWhite && destPiece.isWhite) return false;
				if(!isWhite && !destPiece.isWhite) return false;
				if(vertical == true){
					if(up == true){ //checks vertically moving upwards; makes sure nothing is in the way
						for(int i = srcY-1; i > destY; i--){
							if(game.board[i][destX] != null){
								return false;
							}
						}
					}else{ //checks vertically moving downwards; makes sure nothing is in the way
						for(int i = srcY+1; i < destY; i++){
							if(game.board[i][destX] != null){
								return false;
							}
						}
					}
				}else{
					if(right == true){ //checks horizontally moving right; makes sure nothing is in the way
						for(int i = srcX+1; i < destX; i++){
							if(game.board[destY][i] != null){
								return false;
							}
						}
					}else{  //checks horizontally moving left; makes sure nothing is in the way
						for(int i = srcX-1; i > destX; i--){
							if(game.board[destY][i] != null){
								return false;
							}
						}
					}
				}
			}
			return true;
		}else{
			return false;
		}
	}
}