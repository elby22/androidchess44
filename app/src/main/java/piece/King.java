package piece;

import app.Game;

public class King extends Piece {

	public King(boolean isWhite) {
		super(isWhite);
	}


	public String toString(){
		if(isWhite){
			return "wK";
		}
		return "bK";
	}

	/**
	 * Checks if a move is legal given the starting and ending coordinates.
	 * Implements castling. If castling happens, move the rook in here as opposed to in Game.
	 * Utilizes getChecking to protect isFirstMove during the isCheck() method.
	 * @author Alex (aer112)
	 * 
	 * @param src	coords of the starting position of the piece
	 * @param dest	coords of the destination 
	 * @param game	an instance of a game to determine valid moves
	 * @return true if the move if legal
	 * @return false if the move is illegal
	 */
	public boolean legalMove(int[] src, int[] dest, Game game) {
		boolean right, up, vertical;
		int srcX = src[1];
		int srcY = src[0];
		int destX = dest[1];
		int destY = dest[0];
		
		Piece destPiece = game.board[destY][destX];
		
		
		
		//Handles Castling
		if(isFirstMove && srcY == destY){
			if(destX == srcX+2){ //going to the right rook
				for(int i = srcX+1; i < 7; i++){
					Piece tempPiece = game.board[destY][i];
					if(tempPiece != null){
						return false;
					}
				}
				Piece rookPiece = game.board[destY][7];
				if(rookPiece.isFirstMove == false){
					return false;
				}
				
			//	game.board[destY][destX-1] = rookPiece;
			//	game.board[destY][7] = null;
			//	isFirstMove = false;
			//	rookPiece.isFirstMove = false;
				return true;
				
				
			}else if(destX == srcX-2){ //going to the left rook
				for(int i = srcX-1; i > 0; i--){
					Piece tempPiece = game.board[destY][i];
					if(tempPiece != null){
						return false;
					}
				}
				Piece rookPiece = game.board[destY][0];
				if(rookPiece.isFirstMove == false){
					return false;
				}
				
			//	game.board[destY][destX+1] = rookPiece;
			//	game.board[destY][0] = null;
			//	isFirstMove = false;
			//	rookPiece.isFirstMove = false;
				return true;
			}
		}
		//end Castling
		
		if(srcX == destX && srcY == destY){
			return false;
		}
		
		//Attacking same side?
		if(destPiece != null){
			if(isWhite && destPiece.isWhite) return false;
			if(!isWhite && !destPiece.isWhite) return false;
		}
		if(Math.abs(srcY-destY) > 1){
			return false;
		}
		if(Math.abs(srcX-destX) > 1){
			return false;
		}
		if(srcX == destX){ //checks for moves up and down 1 space
			if(srcY != destY+1 && srcY != destY-1){
				return false;
			}
		}else if(srcY == destY){ //checks for moves left or right one space
			if(srcX != destX+1 && srcX != destX-1){
				return false;
			}
		}else if(srcY == destY+1 || srcY == destY-1){ //checks for moves diagonal one space
			if(srcX != destX+1 && srcX != destX-1){
				return false;
			}
		}
		
		if(!game.getChecking()){
			isFirstMove = false;
		}

		return true;
	}

	
	
	
	
	
}