package piece;

import app.Game;

/**
 * Implements the Bishop piece
 * @author Alex (aer11)
 *
 */
public class Bishop extends Piece {

	public Bishop(boolean isWhite) {
		super(isWhite);
	}

	
	
	
	/**
	 * Checks if a move is legal given the starting and ending coordinates.
	 * First checks for diagonality by seeing if the slope between the starting coordinate and
	 * ending coordinate is 1. Also checks if the piece is attacking another piece, and if any pieces 
	 * are in the way of the start and the destination. If attacking a piece, checks if the piece is friendly.
	 * 
	 * @author Alex (aer112)
	 * 
	 * @param src	coords of the starting position of the piece
	 * @param dest	coords of the destination 
	 * @param game	an instance of a game to determine valid moves
	 * @return true if the move if legal
	 * @return false if the move is illegal
	 */
	public boolean legalMove(int[] src, int[] dest, Game game) {
		boolean right, up;
		int srcX = src[1];
		int srcY = src[0];
		int destX = dest[1];
		int destY = dest[0];
		
		//NEED TO CHECK THE ORDER OF [X][Y]
		Piece destPiece = game.board[destY][destX];
		
		if(srcX > destX){
			right = false;
		}else{
			right = true;
		}
		
		if(srcY > destY){
			up = true;
		}else{
			up = false;
		}
		
		//will always be illegal if the x's or y's are the same
		if(srcX == destX || srcY == destY){
			return false;
		}
		
		//checks for diagonallity 
		if(Math.abs(srcX - destX) != Math.abs(srcY - destY)){
			return false;
		}
		
		//not attacking
		if(destPiece == null){
			if(up == true){
				if(right == true){
					//checks that there are no pieces blocking the way for going UP RIGHT
					int j = srcY-1;
					for(int i = srcX+1; i < destX; i++){
						if(game.board[j][i] != null){
							return false;
						}
						j--;
					}
				}else{
					//checks that there are no pieces blocking the way for going UP LEFT
					int j = srcY-1;
					for(int i = srcX-1; i > destX; i--){
						if(game.board[j][i] != null){
							return false;
						}
						j--;
					}
				}
			}else{
				if(right == true){
					//checks that there are no pieces blocking the way for going DOWN RIGHT
					int j = srcY+1;
					for(int i = srcX+1; i < destX; i++){
						if(game.board[j][i] != null){
							return false;
						}
						j++;
					}
				}else{
					//checks that there are no pieces blocking the way for going DOWN LEFT
					int j = srcY+1;
					for(int i = srcX-1; i > destX; i--){			
						if(game.board[j][i] != null){
							return false;
						}
						j++;
					}
				}
			}
		}else{ //attacking
			if(isWhite && destPiece.isWhite) return false;
			if(!isWhite && !destPiece.isWhite) return false;
			if(up == true){
				if(right == true){
					//checks that there are no pieces blocking the way for going UP RIGHT
					int j = srcY-1;
					for(int i = srcX+1; i < destX; i++){
							if(game.board[j][i] != null){
								return false;
							}
						j--;
					}
				}else{
					//checks that there are no pieces blocking the way for going UP LEFT
					int j = srcY-1;
					for(int i = srcX-1; i > destX; i--){
							if(game.board[j][i] != null){
								return false;
							}
						j--;
					}
				}
			}else{
				if(right == true){
					//checks that there are no pieces blocking the way for going DOWN RIGHT
					int j = srcY+1;
					for(int i = srcX+1; i < destX; i++){
						if(game.board[j][i] != null){
							return false;
						}
						j++;
					}
				}else{
					//checks that there are no pieces blocking the way for going DOWN LEFT
					int j = srcY+1;
					for(int i = srcX-1; i > destX; i--){
						if(game.board[j][i] != null){
							return false;
						}
						j++;
					}
				}
			}
		}
		return true;
	}
	
	public String toString(){
		if(isWhite){
			return "wB";
		}
		return "bB";
	}



}