package piece;

import app.Game;

/**
 * Abstract class for a piece. Mainly used for the legalMove method to be overridden by child classes
 * 
 * @author Elby (egb37)
 * @author Alex (aer112)
 */	
public abstract class Piece {
	
	public boolean isWhite;
	public boolean enp = false; 
	boolean isDead = false;
	public boolean isFirstMove = true;
	
	
	public Piece(boolean isWhite){
		this.isWhite = isWhite;
	}

	/**
	 * Determines if this piece can make a legal move to the destination.
	 * CONVENTION: int coord is in the form [y][x]
	 * 
	 * @author Elby (egb37)
	 * @param src	coords of the starting position of the piece
	 * @param dest	coords of the destination 
	 * @param game	an instance of a game to determine valid moves

	 * @return true this piece can legally make the move from src to dest, false otherwise
	 */	
	public abstract boolean legalMove(int[] src, int[] dest, Game game);

}
